import {log} from "./lib/logger";
import initDB from "./lib/db/fs/_init";
import refreshData from "./lib/db/data/_refresh";
import prepareData from "./lib/db/_merge";
import startServer from "./lib/server";

log.verbose("process started", {pid: process.pid});

Promise
  .all([
    initDB(),
    refreshData(),
  ])
  .then(prepareData)
  .then(trapUSR1)
  .then(startServer)
  .catch((err) => {
    log.fatal({err});
    log.close(() => process.exit(1));
  });

function trapUSR1() {
  process.on("SIGUSR2", () => {
    log.info("received refresh signal");
    refreshData()
      .then(prepareData)
      .then(() => log.debug("refresh finished"));
  });
  log.debug("refresh signal handler attached", {signal: "USR2", pid: process.pid});
}
