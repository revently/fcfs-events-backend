# FCFS Events Data

## Installation

Clone this directory and name it *data* within your `fcfs-events` installation. 

## Content modification

1. Insert your desired event contacts into *addresses/entries.json* and define contact groups within *addresses/groups.json*.
2. Define your invitation/access schemas within *events/access.json*.
3. Create events within *events/events.json* as desired.

## Event definition

### Description

Multiple description values will be concatenated. Descriptions are supposed to be HTML (parsers for *txt* and *md* are available by default).

If prefixed with `file:`, the subsequent file-path will be read (relative to *public/*). If the file ends with *txt* or *md*, it will be parsed as such.
You may specify custom parser and use them like `file:md:some-file.xyz`.
