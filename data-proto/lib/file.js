import fs from "fs";
import path from "path";
import MarkdownIt from "markdown-it";

const BASE_DIR = path.join(__dirname, "..", "files");
const PREFIX_REGEX = /^(?:[a-z]+:)*/;
const SUFFIX_REGEX = /\.([a-z]+)$/;

const md = new MarkdownIt();

export {readFile, readJSON, load};

const PARSER = {
  txt(content) { return content.replace(/\n/g, "<br/>"); },
  md(content) { return md.render(content); },
};

async function readFile(file) {
  return await new Promise((resolve, reject) => {
    fs.readFile(file, (err, content) => { if (err == null) { resolve(content); } else { reject(err); } });
  });
}

async function readJSON(file) {
  const content = await readFile(file);
  return JSON.parse(content.toString());
}

async function load(cache, fileId) {
  if (fileId == null) { return fileId; }
  if (cache.hasOwnProperty(fileId)) { return await cache[fileId]; }
  const file = parseFileID(fileId);
  cache[fileId] = readFile(file.path)
    .then((content) => content.toString())
    .then(parseContent.bind(null, file.contentParser));
  return await cache[fileId];
}

function parseFileID(fileId) {
  const prefix = PREFIX_REGEX.exec(fileId)[0];
  let contentParser;
  if (prefix.length) {
    contentParser = prefix.split(":");
    contentParser.pop();
  } else {
    const suffixMatch = SUFFIX_REGEX.exec(fileId);
    contentParser = suffixMatch == null ? [] : [suffixMatch[1]];
  }
  return {
    path: path.join(BASE_DIR, fileId.substring(prefix.length)),
    contentParser,
  };
}

function parseContent(parser, content) {
  for (let i = 0; i < parser.length; i++) {
    const key = parser[i];
    if (PARSER.hasOwnProperty(key)) { content = PARSER[key](content); }
  }
  return content;
}
