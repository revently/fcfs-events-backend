import {load as loadFile} from "./file";

export {prepare};

async function prepare(cache, accessGroups, event) {
  if (event.description) {
    const arr = Array.isArray(event.description) ? event.description : [event.description];
    const result = await Promise.all(arr.map((el) => el.startsWith("file:") ? loadFile(cache, el.substring(5)) : el));
    event.description = result.join("");
  }
  if (typeof event.access === "string") {
    if (!accessGroups.hasOwnProperty(event.access)) {
      throw new Error("Access group '" + event.access + "' not defined.");
    }
    event.access = accessGroups[event.access];
  }
  return event;
}
