export default async (ctx, next) => {
  ctx.request.startTime = Date.now();
  const result = await next();
  ctx.request.endTime = Date.now();
  ctx.set("X-Response-Time", `${ctx.request.endTime - ctx.request.startTime}ms`);
  return result;
};
