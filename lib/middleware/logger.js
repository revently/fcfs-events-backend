import {log} from "../logger";

export default async function (ctx, next) {
  ctx.log = log.child({ctx});
  ctx.log.debug("request received");
  const result = await next();
  ctx.log.trace("request resolved");
  return result;
}
