export default {
  // GROUP-based trigger
  GROUP: {
    INVITATION: "*GRP", // GROUP gets eligible to join
  },
  // ADDRESS status
  ADDRESS: {
    CONFIRMATION: "<ADR", // ADDRESS gets assigned a free slot
    SUCCESS: "+ADR", // no more EVENT leave actions allowed and ADDRESS has a slot assigned
    DISMISS: "-ADR", // no more EVENT leave actions allowed and ADDRESS has no slot assigned
  },
  // EVENT status
  EVENT: {
    FAIL: "-EVT", // no more EVENT join actions allowed, required slots not filled and ADDRESS has a slot assigned
  },
  // ADMIN actions
  ADMIN: {
    MODIFICATION: "[MOD", // admin changes EVENT properties that are visible on display
    CANCEL: "[DEL", // admin deletes EVENT
  },
};
