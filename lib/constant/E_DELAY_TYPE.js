const TIME_VALUES = [1, 1000, 60000, 3600000, 86400000];

const VALUES = {
  // time delay
  MILLISECONDS: 0,
  SECONDS: 1,
  MINUTES: 2,
  HOURS: 3,
  DAYS: 4,
  // entry-based delay
  ENTRIES: 5,
  ENTRIES_RELATIVE: 6,
};
const MAX_VALUE = 6;

export default VALUES;

export function isTimeTriggered(type) {
  if (type == null) { return true; }
  const idx = typeof type === "string" ? VALUES[type] : type;
  return idx < TIME_VALUES.length;
}

export function getMilliseconds(value, type) {
  return value * TIME_VALUES[type == null ? VALUES.HOURS : typeof type === "string" ? VALUES[type] : type];
}

export function exists(type) {
  if (type == null) { return true; }
  if (typeof type === "string") { return VALUES.hasOwnProperty(type); }
  if (typeof type === "number") { return type <= MAX_VALUE; }
  return false;
}
