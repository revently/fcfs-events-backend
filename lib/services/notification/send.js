import {template} from "lodash";

import config from "../../../config";
import {sendMail} from "../mailer";
import {tt} from "../translations";
import {byId as addressById} from "../../db/data/address";
import {getAddresses as addressesByGroupID} from "../../db/data/group";
import {access as tokenDB, addressIds, create as createToken, resolveToken} from "../../db/fs/token";
import {access as notificationDB, add as addNotification, find as findNotification} from "../../db/fs/notification";
import {mapAsync, mapDeep} from "../../util/misc";
import TYPE from "../../constant/E_NOTIFICATION_TYPE";
import { humanDiff } from "../event";

export {modification, invitations, confirm, cancel, fail, success, dismiss};

/*==================================================== Functions  ====================================================*/

/*------------------------------------------- Group specific notifications -------------------------------------------*/

async function invitations(event, groupId) {
  const addresses = addressesByGroupID(groupId);
  return await (
    genericSend(TYPE.GROUP.INVITATION, event, addresses, (address) => {
      if (event.schedule.close <= Date.now()) { throw new Error("Invitation mail not allowed for closed event."); }
      return defaultEmailData(event, address, createToken(event._id, address._id));
      // todo if sending fails, remove created token
    })
      .finally(tokenDB.write.bind(tokenDB))
  );
}

/*------------------------------------------ Address specific notifications ------------------------------------------*/

async function confirm(event, entries) {
  return await genericSend(TYPE.ADDRESS.CONFIRMATION, event, entries.map(addressById), (address) => {
    if (
      findNotification(event._id, address._id, TYPE.ADDRESS.SUCCESS) != null ||
      hasRejectionNotification(event, address)
    ) { throw new Error("Confirm not allowed after success/rejection."); }
    return defaultEmailData(event, address, resolveToken(event._id, address._id));
  });
}

async function success(event, entries) {
  return await genericSend(TYPE.ADDRESS.SUCCESS, event, entries.map(addressById), (address) => {
    if (event.schedule.stick > Date.now()) { throw new Error("Success mail not allowed for sticky event."); }
    return defaultEmailData(event, address, resolveToken(event._id, address._id));
  });
}

async function dismiss(event, entries) {
  return await genericSend(TYPE.ADDRESS.DISMISS, event, entries.map(addressById), (address) => {
    if (event.schedule.stick > Date.now()) { throw new Error("Dismiss mail not allowed for sticky event."); }
    if (hasRejectionNotification(event, address)) { throw new Error("Dismiss mail not allowed after rejection."); }
    return defaultEmailData(event, address, resolveToken(event._id, address._id));
  });
}

/*------------------------------------------- Event specific notifications -------------------------------------------*/

async function fail(event, entries) {
  return await genericSend(TYPE.EVENT.FAIL, event, entries.map(addressById), (address) => {
    if (event.schedule.close > Date.now()) { throw new Error("Fail mail not allowed for closed event."); }
    if (hasRejectionNotification(event, address)) { throw new Error("Fail mail not allowed after rejection."); }
    return defaultEmailData(event, address, resolveToken(event._id, address._id));
  });
}

/*------------------------------------------ Admin triggered notifications  ------------------------------------------*/

async function modification(event, diff) {
  diff = await humanDiff(diff);
  const diffText = diff.map(stringifyDiffEntry.bind(null, "\n")).join("\n");
  const diffHTML = diff.map(stringifyDiffEntry.bind(null, "<br/>")).join("<br/>");
  return await genericSend(
    TYPE.ADMIN.MODIFICATION,
    event,
    addressIds(event._id).map(addressById),
    (address) => {
      if (
        findNotification(event._id, address._id, TYPE.ADDRESS.SUCCESS) != null ||
        hasRejectionNotification(event, address)
      ) { throw new Error("Modification mail not allowed after success/rejection."); }
      return {
        ...defaultEmailData(event, address, resolveToken(event._id, address._id)),
        diff: {text: diffText, html: diffHTML},
      };
    },
    true
  );
}

async function cancel(event, entries) {
  return await genericSend(TYPE.ADMIN.CANCEL, event, entries.map(addressById), (address) => {
    if (event.schedule.start <= Date.now()) { throw new Error("Cancel mail not allowed for started event."); }
    if (hasRejectionNotification(event, address)) { throw new Error("Cancel mail not allowed after rejection."); }
    return defaultEmailData(event, address, resolveToken(event._id, address._id));
  });
}

/*------------------------------------------------------- Misc -------------------------------------------------------*/

async function genericSend(type, event, addresses, iteratee, ignorePast) {
  if (!addresses.length) { return; }
  const templateFns = mapDeep(await tt("mail.event." + type, {
    subject: "subject",
    content: {
      text: "content.text",
      html: "content.html",
    },
  }), template);
  const id = ignorePast ? `${type}::${Date.now()}` : type;
  await mapAsync(addresses, async (address) => {
    const notification = findNotification(event._id, address._id, id);
    if (notification != null) { return; }
    const data = iteratee(address);
    await sendMail(address, {
      config,
      data,
      subject: templateFns.subject(data),
      content: {
        text: templateFns.content.text(data),
        html: templateFns.content.html(data),
      },
    });
    addNotification(event._id, address._id, id);
    await notificationDB.write();
  });
}

function defaultEmailData(event, address, token) {
  return {
    event: event.definition,
    address,
    token,
    link: `${config.url}/event/${event._id}?token=${token}`,
  };
}

function hasRejectionNotification(event, address) {
  return findNotification(event._id, address._id, TYPE.EVENT.FAIL) != null ||
    findNotification(event._id, address._id, TYPE.ADDRESS.DISMISS) != null ||
    findNotification(event._id, address._id, TYPE.ADMIN.CANCEL) != null;
}

function stringifyDiffEntry(separator, value) {
  let prevValue = JSON.stringify(value[1]), newValue = JSON.stringify(value[2]);
  prevValue = prevValue.replace(/</g, "&lt;").replace(/>/g, "&gt;");
  newValue = newValue.replace(/</g, "&lt;").replace(/>/g, "&gt;");
  if (prevValue.length > 60) { prevValue = prevValue.substring(0, 60) + " [...]"; }
  if (newValue.length > 60) { newValue = newValue.substring(0, 60) + " [...]"; }
  return `  ${value[0]}:${separator}    - ${prevValue}${separator}    + ${newValue}`;
}
