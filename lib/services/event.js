import {defaultsDeep} from "lodash";

import DEFAULT_OPTIONS from "../constant/DEFAULT_OPTIONS_EVENT";
import {isExposed} from "./exposure";
import { t } from "./translations";

const DATE_TRANSFORM = timestamp => new Date(timestamp).toLocaleString("de");
const DIFF_HUMAN_TRANSFORM = {
  "schedule.close": DATE_TRANSFORM,
  "schedule.stick": DATE_TRANSFORM,
  "schedule.start": DATE_TRANSFORM,
};

export {prepare, getEventStatus, getSelfStatus, getExposure, getDisplayExposedProperties, humanDiff};

/*==================================================== Functions  ====================================================*/

function prepare(event) {
  const options = defaultsDeep(event.options, DEFAULT_OPTIONS);
  const definition = Object.assign({}, event, {options});
  const startDate = Date.parse(event.date);
  return {
    definition,
    cdate: new Date(),
    schedule: {
      close: startDate - (options.freezeTime.join ? options.freezeTime.join * 3600000 : 0),
      stick: startDate - (options.freezeTime.leave != null ? options.freezeTime.leave * 3600000 : 0),
      start: startDate,
      remove: options.keepOnline == null ? null : options.keepOnline * 3600000 + startDate,
    },
  };
}

function getEventStatus(event, entries, now) {
  const eventOpt = event.definition.options;
  const eventSufficed = eventOpt.minEntries == null || entries >= eventOpt.minEntries;
  return {
    sufficed: eventSufficed,
    closed: now >= event.schedule.close,
    sticky: now >= event.schedule.stick,
    removed: event.schedule.remove != null && now >= event.schedule.remove,
  };
}

function getSelfStatus(event, addressId, addressIndex) {
  const eventOpt = event.definition.options;
  const selfJoined = addressIndex >= 0;
  const selfInSlot = selfJoined && (eventOpt.maxEntries == null || addressIndex < eventOpt.maxEntries);
  return {
    invited: addressId != null,
    joined: selfJoined,
    assigned: selfInSlot,
  };
}

function getExposure(event, status) {
  const exposureOpt = event.definition.options.exposure;
  return {
    base: isExposed(exposureOpt.base, status),
    display: isExposed(exposureOpt.display, status),
    assigned: isExposed(exposureOpt.assigned, status),
    entries: isExposed(exposureOpt.entries, status),
    slots: isExposed(exposureOpt.slots, status),
    queue: isExposed(exposureOpt.queue, status),
  };
}

function getDisplayExposedProperties(event) {
  return {
    _id: event._id,
    title: event.definition.title,
    description: event.definition.description,
    options: {
      entries: {min: event.definition.options.minEntries, max: event.definition.options.maxEntries},
    },
    schedule: {
      close: event.schedule.close,
      stick: event.schedule.stick,
      start: event.schedule.start,
    },
  };
}

async function humanDiff(diff) {
  return Promise.all(diff.map(([key, oldValue, newValue]) => {
    if (DIFF_HUMAN_TRANSFORM.hasOwnProperty(key)) {
      const transform = DIFF_HUMAN_TRANSFORM[key];
      oldValue = transform(oldValue);
      newValue = transform(newValue);
    }
    return t(`event.${key}`).then(it => [it, oldValue, newValue]);
  }));
}
