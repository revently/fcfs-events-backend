import {get, mapKeys} from "lodash";
import path from "path";

import config from "../../config";
import {mapAsync} from "../util/misc";
import {readDir, readJSON} from "../util/file";

export {getOne as t, getMany as tt};

const BASE_DIR = path.join(__dirname, "..", "..", "translations", config.translations);
const FALLBACK_DIR = path.join(__dirname, "..", "..", "translations", "en");

const promise = init();

/*==================================================== Functions  ====================================================*/

async function getMany(prefix, keys) {
  if (keys == null) {
    keys = prefix;
    prefix = null;
  }
  if (typeof keys === "string") { return await getOne(prefix, keys); }
  return await mapAsync(keys, getMany.bind(null, prefix));
}

async function getOne(prefix, key) {
  if (key == null) { key = prefix; } else if (prefix != null) { key = prefix + "." + key; }
  const data = await promise;
  const keyPath = toPath(key);
  return get(data.base, keyPath) || get(data.fallback, keyPath);
}

async function init() {
  const [base, fallback] = await Promise.all([
    readTranslations(BASE_DIR),
    readTranslations(FALLBACK_DIR),
  ]);
  return {base, fallback};
}

async function readTranslations(dir) {
  const files = await readDir(dir);
  return await mapAsync(
    mapKeys(files, (name) => path.basename(name, ".json")),
    (file) => readJSON(path.join(dir, file))
  );
}

function toPath(key) {
  if (typeof key === "string") { return key.split("."); }
  if (Array.isArray(key)) { return key; }
  throw new Error("Invalid key type: " + typeof key);
}
