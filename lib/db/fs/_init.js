import {access as eventDB} from "./event";
import {access as notificationDB} from "./notification";
import {access as entryDB} from "./entry";
import {access as tokenDB, init as initToken} from "./token";

export default async function refresh() {
  return await Promise
    .all([
      tokenDB.read().then(initToken),
      eventDB.read(),
      entryDB.read(),
      notificationDB.read(),
    ]);
}
