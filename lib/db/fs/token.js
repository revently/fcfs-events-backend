import {invert, keys, mapValues} from "lodash";
import {randomBytes} from "crypto";

import {token as db} from "./_access";

export {db as access, init, resolve, resolveToken, addressIds, create, remove};

let inverse = {};

/*==================================================== Functions  ====================================================*/

async function init() { inverse = mapValues(db.data, invert); }

function resolve(eventId, token) {
  if (!db.data.hasOwnProperty(eventId) || token == null) { return null; }
  return db.data[eventId] && db.data[eventId][token];
}

function resolveToken(eventId, addressId) {
  if (!inverse.hasOwnProperty(eventId) || addressId == null) { return null; }
  return inverse[eventId] && inverse[eventId][addressId];
}

function addressIds(eventId) {
  if (!inverse.hasOwnProperty(eventId)) { return []; }
  return keys(inverse[eventId]);
}

function create(eventId, addressId) {
  if (!db.data.hasOwnProperty(eventId)) { db.data[eventId] = {}; }
  if (!inverse.hasOwnProperty(eventId)) { inverse[eventId] = {}; }
  const token = randomBytes(4).toString("hex");
  db.data[eventId][token] = addressId;
  inverse[eventId][addressId] = token;
  db.changed();
  return token;
}

function remove(eventId) {
  if (!db.data.hasOwnProperty(eventId)) { return false; }
  Reflect.deleteProperty(db.data, eventId);
  Reflect.deleteProperty(inverse, eventId);
  db.changed();
  return true;
}
