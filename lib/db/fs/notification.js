import {findLast, reject} from "lodash";

import {notifications as db} from "./_access";

export {db as access, find, add, removeAll};

/*==================================================== Functions  ====================================================*/

function find(eventId, addressId, type) {
  if (!db.data.hasOwnProperty(eventId)) { return null; }
  return findLast(db.data[eventId], {address: addressId, type});
}

function add(eventId, addressId, type, data) {
  if (!db.data.hasOwnProperty(eventId)) { db.data[eventId] = []; }
  db.data[eventId].push({
    date: Date.now(),
    address: addressId,
    type,
    data,
  });
  db.changed();
}

function removeAll(eventId, predicate) {
  if (!db.data.hasOwnProperty(eventId)) { return false; }
  if (predicate === void 0) {
    Reflect.deleteProperty(db.data, eventId);
  } else {
    db.data[eventId] = reject(db.data[eventId], predicate);
  }
  db.changed();
  return true;
}
