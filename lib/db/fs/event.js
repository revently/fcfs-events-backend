import {events as db} from "./_access";

export {db as access, getAll, byId, add, add as update, remove};

/*==================================================== Functions  ====================================================*/

function getAll() { return db.data; }

function byId(eventId) { return db.data[eventId]; }

function add(event) {
  db.data[event._id] = event;
  db.changed();
}

function remove(eventId) {
  if (!db.data.hasOwnProperty(eventId)) { return false; }
  Reflect.deleteProperty(db.data, eventId);
  db.changed();
  return true;
}
