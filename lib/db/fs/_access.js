import path from "path";
import {mkdirSync} from "fs";
import {stubObject} from "lodash";

import config from "../../../config";
import {readJSON, writeJSON} from "../../util/file";

const BASE_PATH = path.resolve(config.paths.base, config.paths.db);
const NOTIFICATIONS_PATH = path.join(BASE_PATH, "notifications.json");
const EVENTS_PATH = path.join(BASE_PATH, "events.json");
const ENTRIES_PATH = path.join(BASE_PATH, "entries.json");
const TOKEN_PATH = path.join(BASE_PATH, "token.json");

/*================================================ Initial execution  ================================================*/

// eslint-disable-next-line no-empty
try { mkdirSync(BASE_PATH); } catch (ignored) {}

/*==================================================== Functions  ====================================================*/

export const notifications = createDBAccess(NOTIFICATIONS_PATH, stubObject);
export const events = createDBAccess(EVENTS_PATH, stubObject);
export const entries = createDBAccess(ENTRIES_PATH, stubObject);
export const token = createDBAccess(TOKEN_PATH, stubObject);

function createDBAccess(path, fallbackCb) {
  return {
    data: null,
    _changed: true,
    _writeLock: Promise.resolve(),
    changed() {
      this._changed = true;
      return this;
    },
    async read() {
      this.data = await readJSON(path).catch(fallbackCb);
      this._changed = false;
      return this.data;
    },
    async write() {
      if (!this._changed) { return await this._writeLock; }
      this._changed = false;
      const _write = () => writeJSON(path, this.data);
      await (this._writeLock = this._writeLock.then(_write, _write));
    },
  };
}
