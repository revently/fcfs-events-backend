import {addresses, events, groups} from "../../../data";

export const getAddresses = addresses;
export const getGroups = groups;
export const getEvents = events;
