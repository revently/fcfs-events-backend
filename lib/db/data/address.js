import {getAddresses} from "./_access";
import {log} from "../../logger";
import {each} from "lodash";

export {refresh, byId};

let addressObj = null;

/*==================================================== Functions  ====================================================*/

async function refresh() {
  addressObj = await (typeof getAddresses === "function" ? getAddresses() : getAddresses);
  if (typeof addressObj !== "object" || addressObj === null) { throw new Error("Invalid addresses definition."); }
  each(addressObj, prepareAddress);
  log.verbose("addresses read");
  return addressObj;
}

function prepareAddress(entry, id) { entry._id = id; }

function byId(addressId) { return addressObj[addressId]; }
