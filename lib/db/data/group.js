import {getGroups} from "./_access";
import {log} from "../../logger";
import {each} from "lodash";

export {refresh, getAddresses};

let groupMap = null;
let addressMap = null;

/*==================================================== Functions  ====================================================*/

async function refresh(addresses) {
  groupMap = await (typeof getGroups === "function" ? getGroups() : getGroups);
  if (!(groupMap instanceof Map)) { throw new Error("Invalid groups definition."); }
  addressMap = new Map();
  each(addresses, addAddress);
  log.verbose("groups read");
}

function addAddress(entry) {
  each(entry.groups, (groupId) => {
    if (!addressMap.has(groupId)) { addressMap.set(groupId, []); }
    addressMap.get(groupId).push(entry);
  });
}

function getAddresses(groupId) { return addressMap.get(groupId) || []; }
