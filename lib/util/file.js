import fs from "fs";

export {readJSON, writeJSON, readDir};

/*==================================================== Functions  ====================================================*/

async function readFile(file) {
  return await new Promise((resolve, reject) => {
    fs.readFile(file, (err, content) => { if (err == null) { resolve(content); } else { reject(err); } });
  });
}

async function writeFile(file, content) {
  return await new Promise((resolve, reject) => {
    fs.writeFile(file, content, (err) => { if (err == null) { resolve(); } else { reject(err); } });
  });
}

async function readJSON(file) {
  const content = await readFile(file);
  return JSON.parse(content.toString());
}

async function writeJSON(file, data) {
  await writeFile(file, JSON.stringify(data));
}

async function readDir(dir) {
  return await new Promise((resolve, reject) => {
    fs.readdir(dir, (err, files) => { if (err == null) { resolve(files); } else { reject(err); } });
  });
}
